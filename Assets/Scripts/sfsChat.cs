﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Sfs2X.Entities;
using UnityEngine.Networking;
//using SimpleJSON;
using UnityEngine.SceneManagement;

public class sfsChat : MonoBehaviour {
    private static sfsChat SfsCHat;
    public static sfsChat _SfsCHat
    {
        get
        {
            if (SfsCHat == null)
            {

                GameObject sfsobject = new GameObject("sfs");
                SfsCHat = sfsobject.AddComponent<sfsChat>();
                DontDestroyOnLoad(sfsobject.gameObject);
            }
            return SfsCHat;
        }
    }
    SmartFox sfs;
    public ISFSObject sfsobj;
   public string Ip = "192.168.137.1";
    public string port = "9933";
    string senderMessage ;
    private string ZoneName = "BasicExamples";
    public string username ;
	private string message="UserAdded" ;
    private string ReceivingMsg;

    float timer;
    public void getdata(string Message)
	{
        //Debug.Log (message);
        message = Message.ToString();
        //  Init();
       // SendData();
        
    }
	 void Start () {
   

        sfsEvents();
        Debug.Log(sfs.ToString());
    }
	

	void Update () {
        sfs.ProcessEvents();
     //   timer+=Time.deltaTime;
       // Debug.Log(timer);
        if (timer >30)
        {
            if (sfs.IsConnected)
            {
                
                sfs.Disconnect();
            }
        }
       
        
    }

    void sfsEvents()
    {
        sfs = new SmartFox();
        sfs.AddEventListener(SFSEvent.CONNECTION, Onconnection);
        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, ConnectionLost);      
         sfs.AddEventListener(SFSEvent.CONNECTION_RETRY, Retry);
         sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
         sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
      sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, onExtensionResponse);
      //  sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, Onpingpong);
        sfs.AddEventListener(SFSEvent.PUBLIC_MESSAGE, OnpublicMessage);
     //   sfs.AddEventListener(SFSEvent.ROOM_ADD, onRoomAdded);
     //   sfs.AddEventListener(SFSEvent.ROOM_CREATION_ERROR, onRoomCreationError);
         sfs.AddEventListener(SFSEvent.ROOM_JOIN, onJoin);
         sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, onJoinError);
         SfsConnect();
    }
    void SfsConnect()
    {
    
        sfsobj = new SFSObject();
        sfs.Connect(Ip, int.Parse(port));
        Debug.Log("Conected");
    }

    void Onconnection(BaseEvent evt)
    {
        if ((bool)evt.Params["success"])
        {
            Debug.Log("Connection established successfully");
            Debug.Log("SFS2X API version: " + sfs.Version);
            Debug.Log("Connection mode is: " + sfs.ConnectionMode);
            sfs.Send(new LoginRequest("","",ZoneName));
        }
        else
        {
            ConnectionLost(evt);
        }

    }
    void ConnectionLost(BaseEvent evt)
    {
        Debug.Log("Connection was lost reason is: " + (string)evt.Params["reason"]);
  
        Debug.Log(sfs.GetReconnectionSeconds());
    }
    void Retry(BaseEvent evt)
    {
        sfs.Connect(Ip, int.Parse(port));
    }
   void OnLogin(BaseEvent evt)
       {
        Debug.Log("Logged In: " + evt.Params["user"]);       
        username= (string)evt.Params["user"].ToString(); 
        sfs.Send(new JoinRoomRequest("The Lobby"));
        movemnt.Movement.userConfigure(username);
    }


       void OnLoginError(BaseEvent evt)
        {
            Debug.Log("Login error: (" + evt.Params["errorCode"] + "): " + evt.Params["errorMessage"]);
         }
  /*   void  SendData()
    {
        Debug.Log(sfs.ToString());  
        if (sfs != null)
        {
            sfs.Send(new PublicMessageRequest(message));
        }
        else
        {
            Debug.Log("sfs null");
        }
    }*/
   void OnpublicMessage(BaseEvent evt)
    {
       
        Debug.Log("message Sent");
     //   User sender = (User)evt.Params["sender"];
      //  Debug.Log(sender.Name + ":" + message);

        // string message = (string)evt.Params["message"];

        List<Room> roomList = sfs.GetRoomListFromGroup("default");
        //  List<User> userList = sfs.GetRoomByName("BasicExample").UserList;
        // User userL=userL.GetPlayerId.c
        foreach (Room room in roomList)
        {
            List<User> userList = room.UserList;
            Debug.Log(userList.Count);
            foreach (User user in userList)
            {
                if (user.Name != username)
                {
                    movemnt.Movement.UsersList(userList.Count);
                }


            }
            }
          
            //  if (username != sender.Name)
            //  {
            // ReceivingMsg = (string)evt.Params["message"];
            // ChatHandler._CHatHandler.recieverMessage(ReceivingMsg);
            // }
            //   else if (username == sender.Name)
            //  {
            //  senderMessage = (string)evt.Params["message"];
            //ChatHandler._CHatHandler.senderMessage(senderMessage);
            // }
        }
    void onExtensionResponse(BaseEvent evt)
    {

    }
     void onJoin(BaseEvent evt)
     {
        sfs.Send(new PublicMessageRequest("useradded"));
        Debug.Log ("Joined Room: " + evt.Params["room"]);

     
        }
       // Debug.Log(userList);
      

    
     void onJoinError(BaseEvent evt)
     {
        Debug.Log ("Join failed: " + evt.Params["errorMessage"]);
     }
    void OnApplicationQuit()
    {
        sfs.Disconnect();
    }
    public void playertransformLive(NetworkTransform PlayerPos)
    {
        movemnt.Movement.PlayerPrefab.transform.position = PlayerPos.transform.position;
      //  PlayerPos.ToSFSObject(sfsobj);
            }
}
